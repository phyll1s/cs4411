// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 12/1/2010                                       
// PROGRAM ASSIGNMENT 5                                        
// FILE NAME : prog5.c           
// PROGRAM PURPOSE :                                           
//    A fake cpu scheduler that uses the UNIX alarm function
//    to simulate a round robin scheduling system.       
// ----------------------------------------------------------- 

#include "funct5.c"
#include <unistd.h>
#include <signal.h>
#include  <setjmp.h>

void alarmHandler(int);
void ctrlCHandler(int);

jmp_buf   jumpBuffer;

int quantum,numFunks,currFunk;
int functions[5] = {1,2,3,4,5};
int functionHealth[5] = {1,1,1,1,1};

// ----------------------------------------------------------- 
// FUNCTION  main                         
//     Control the execution of the program                            
// PARAMETER USAGE :                                           
//    none               
// FUNCTION CALLED :                                           
//    no functions called explicity         
// ----------------------------------------------------------- 
main(){
    //Default values    
    quantum = 1;
    numFunks = 5;
    currFunk = -1;

    //Install the alarm&ctrl-c signal handlers
    signal(SIGALRM, alarmHandler);
    signal(SIGINT, ctrlCHandler);

    while(1){
        //Set jump return point
        sigsetjmp(jumpBuffer,1);

        currFunk = (currFunk + 1) % numFunks;

        //Iterate through functions until a live one is found
        while(functionHealth[functions[currFunk]-1] != 1){
            currFunk = (currFunk + 1) % numFunks;
        }
        
        alarm(quantum);
        if(functions[currFunk]==1){
            f1();
        }
        else if(functions[currFunk]==2){
            f2();
        }
        else if(functions[currFunk]==3){
            f3();
        }
        else if(functions[currFunk]==4){
            f4();
        }
        else if(functions[currFunk]==5){
            f5();
        }              
    }    
}

// ----------------------------------------------------------- 
// FUNCTION  alarmHandler
//     Handle alarm signals                            
// PARAMETER USAGE :                                           
//    int sig - the signal caught               
// FUNCTION CALLED :                                           
//    none          
// ----------------------------------------------------------- 
void alarmHandler(int sig){
    signal(sig, SIG_IGN);
    signal(SIGALRM, alarmHandler);
    siglongjmp(jumpBuffer,1);
}

// ----------------------------------------------------------- 
// FUNCTION  ctrlCHandler
//     Handle ctrl-c signals                            
// PARAMETER USAGE :                                           
//    int sig - the signal caught               
// FUNCTION CALLED :                                           
//    none          
// ----------------------------------------------------------- 
void ctrlCHandler(int sig){
    int timeLeft;
    timeLeft = alarm(0);
    
    //Ignore alarm & ctrl-c that go off
    signal(SIGALRM, SIG_IGN);
    signal(SIGINT, SIG_IGN);
    
    int buffSize1,buffSize2;
    buffSize1 = 1;
    buffSize2 = 2;
    char buff1[buffSize1],buff2[buffSize2];
    
    printf("\nShell -> ");
    fflush(stdout);
    
    while(1){
        if(read(STDIN_FILENO,buff1,buffSize1)>0){
            
            //*********Exit on 'e'*******//
            if(toupper(buff1[0]) == 'E'){
                exit(1);
            }
            
            //*********Kill function on 'k'*******//
            else if(toupper(buff1[0]) == 'K'){            
                read(STDIN_FILENO,buff2,buffSize2);

                //If function is running or suspended, kill it
                if(functionHealth[atoi(&buff2[1])-1] != 0){
                    printf("Function %c has been killed.\n",buff2[1]);
                    functionHealth[atoi(&buff2[1])-1] = 0;
                }else{
                    printf("Function %c does not exist.\n",buff2[1]);
                }
            }
            
            //*********Resume on 'r'*******//
            else if(toupper(buff1[0]) == 'R'){
                //Resume function if still alive
                if(functionHealth[functions[currFunk-1]] == 1){
                    if(timeLeft != 0){
                        signal(SIGINT, ctrlCHandler);
                        signal(SIGALRM, alarmHandler);
                        alarm(timeLeft);
                        return;
                    }else{
                        signal(SIGINT, ctrlCHandler);
                        signal(SIGALRM, alarmHandler);
                        raise(SIGALRM);
                        return;
                    }
                }
                else{
                    //Check of living functions
                    int available=0,i;
                    for(i=0;i<numFunks;i++){
                        if(functionHealth[i] == 1)
                            available = 1;
                    }
                    if(available){
                        signal(SIGINT, ctrlCHandler);
                        signal(SIGALRM, alarmHandler);
                        raise(SIGALRM);
                        return;
                    }
                    else
                        printf("No running functions exist\n");
                }
            }
            
            //*********Suspend function on 'c'*******//
            else if(toupper(buff1[0]) == 'S'){
                read(STDIN_FILENO,buff2,buffSize2);

                //If function is running, suspend it
                if(functionHealth[atoi(&buff2[1])-1] == 1){
                    printf("Function %c has been suspended.\n",buff2[1]);
                    functionHealth[atoi(&buff2[1])-1] = 2;
                }
                //If function is suspended already
                else if(functionHealth[atoi(&buff2[1])-1] == 2){
                    printf("Function %c was suspended\n",buff2[1]);
                }
                //If function is killed or nonexistent
                else if(functionHealth[atoi(&buff2[1])-1] == 0){
                    printf("Function %c does not exist.\n",buff2[1]);
                }
            }
            
            //*********Activate function on 'a'*******//
            else if(toupper(buff1[0]) == 'A'){
                read(STDIN_FILENO,buff2,buffSize2);

                //If function is suspended, activate it
                if(functionHealth[atoi(&buff2[1])-1] == 2){
                    printf("Function %c has been activated\n",buff2[1]);
                    functionHealth[atoi(&buff2[1])-1] = 1;
                }
                //If function is active/dead/nonexistant
                else{
                    printf("Function %c is not a suspended one.\n",buff2[1]);
                }
            }
            
            //*********Change order of execution on 'o'*******//
            else if(toupper(buff1[0]) == 'O'){
                int i,j,vFunks;
                int temp[5];
                char buff3[10];
                vFunks = 0; j = 0;
                
                read(STDIN_FILENO,buff3,10);
                
                for(i=1;i<10;i=i+2){
                    if(functionHealth[atoi(&buff3[i])-1] == 0)
                        printf("Function %c was killed. Assigned order ignored.\n",buff3[i]);
                    else if(functionHealth[atoi(&buff3[i])-1] == 2)
                        printf("Function %c was suspended.  Assigned order ignored.\n",buff3[i]);
                    else{
                        temp[vFunks] = atoi(&buff3[i]);
                        vFunks++;
                    }
                    functions[j] = atoi(&buff3[i]);
                    j++;
                }
                printf("The execution order is changed to ");
                for(i=0;i<vFunks;i++)
                    printf("%i ",temp[i]);
                printf("\n");
            }
            
            //*********Set time quantum on 't'*******//
            else if(toupper(buff1[0]) == 'T'){
                read(STDIN_FILENO,buff2,buffSize2);
                quantum = atoi(&buff2[1]);
                if(quantum > timeLeft)
                    timeLeft = quantum - timeLeft;
                else
                    timeLeft = 0;
                                        
                printf("Time quantum has been set to %c seconds\n",buff2[1]);
            }
        }
        //Read to fix double prompt issue
        read(STDIN_FILENO,buff1,buffSize1);
        printf("Shell -> ");
        fflush(stdout);     
    }
    
    //Re-install the alarm&ctrl-c signal handlers
    signal(SIGINT, ctrlCHandler);
    signal(SIGALRM, alarmHandler);
}

