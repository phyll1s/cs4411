// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 11/10/2010                                       
// PROGRAM ASSIGNMENT 4                                        
// FILE NAME : main.c           
// PROGRAM PURPOSE :                                           
//    Matrix Multiplication with Unix Shared Memory      
// ----------------------------------------------------------- 

#include  <sys/types.h>
#include  <sys/ipc.h>
#include  <sys/shm.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

void cleanUp(int* memPtr, int memId);

// ----------------------------------------------------------- 
// FUNCTION  main :                         
//     find sum of matrix multiplication w/ multi. processes                           
// PARAMETER USAGE :                                           
//    none.               
// FUNCTION CALLED :                                           
//    cleanUp          
// ----------------------------------------------------------- 
main(){
    key_t sharedMemKey;
    size_t sharedMemSize;
    int sharedMemId;
    int *sharedMemPtr;
    int l,m,u,v;
    
    printf("Matrix multiplication with multiple processes:\n\n");
   
    //Set key
    sharedMemKey = 2010;
    printf("*** MAIN: shared memory key = %d\n", sharedMemKey);
    
    //Get shared memory
    sharedMemSize = ((200 + 100 + 4)*sizeof(int));
    sharedMemId = shmget(sharedMemKey,sharedMemSize,IPC_CREAT | 0666);
    if (sharedMemId < 0) {
        printf("shmget error:%d\n",sharedMemId);
        cleanUp(sharedMemPtr,sharedMemId);
        exit(1);
    }
    printf("*** MAIN: shared memory created\n");

    //Attached shared memory to address space
    sharedMemPtr = (int *) shmat(sharedMemId, NULL, 0);  /* attach */
    if ((int) sharedMemId == -1) {
         printf("*** shmat error (server) ***\n");
         cleanUp(sharedMemPtr,sharedMemId);
         exit(1);
    }
    printf("*** MAIN: shared memory attached and is ready to use\n\n");
  
    //Read arrays into shared memory
    //Read array A dimensions
    scanf("%d",&sharedMemPtr[0]);
    scanf("%d",&sharedMemPtr[1]);
    l = sharedMemPtr[0];
    m = sharedMemPtr[1];

    //Read array A
    int i;
    for(i=2; i<(l*m+2); i++){
       scanf("%d",&sharedMemPtr[i]); 
    }

    //Read array B dimensions
    scanf("%d",&sharedMemPtr[l*m+3]);
    scanf("%d",&sharedMemPtr[l*m+4]);
    u = sharedMemPtr[l*m+3];
    v = sharedMemPtr[l*m+4];
    
    //Read array B
    for(i=(l*m+5); i<(l*m+5 + u*v); i++){
       scanf("%d",&sharedMemPtr[i]); 
    }
    
    //Check if arrays are proper dimension
    if(m!=u){
        printf("Arrays not proper dimensions - exiting!\n");
        cleanUp(sharedMemPtr,sharedMemId);
        exit(1);
    }
      
    //Print original arrays
    printf("Matrix A: %d rows and %d columns\n", l,m);
    for(i=0;i<l;i++){
        int j;
        for(j=0;j<m;j++){
            printf("%*d",5,sharedMemPtr[(2+j+m*i)]);
        }
        printf("\n");    
    }
    printf("\n");
    
    printf("Matrix B: %d rows and %d columns\n", u,v);
    for(i=0;i<u;i++){
        int j;
        for(j=0;j<v;j++){
            printf("%*d",5,sharedMemPtr[(5+l*m+v*i+j)]);
        }
        printf("\n");    
    }    
    printf("\n");    
    
    //Create l*n processes, one for each entry of the product matrix C = A*B
    char* binName;
    binName = "./compute";
      
    printf("*** MAIN: about to spawn processes\n\n");
    
    for(i=0;i<l;i++){
        int r;
        for(r=0;r<v;r++){
            int* t1 = &i;
            int* t2 = &r;
            char *const argv[] = { "./compute", t1, t2, (char *)0 };
                 
            //Child process executes compute binary
            if (fork() == 0) {
                execvp("./compute", argv);
                exit(1);
            }
        }
    }
    
    //Wait for processes to finish
    for(i=0;i<l;i++){
        int j;
        for(j=0;j<v;j++){
            wait();
        }
    }    
    
    //Print out result
    printf("\n"); 
    printf("Matrix C: %d rows and %d columns\n",l,v);
    for(i=0;i<l;i++){
        int j;
        for(j=0;j<v;j++){
            printf("%*d",5,sharedMemPtr[5+l*m+u*v+i*v+j]);
        }
        printf("\n");    
    }    
    printf("\n"); 
    
      
    //Cleanup and exit
    cleanUp(sharedMemPtr,sharedMemId);
    exit(1);
}

// ----------------------------------------------------------- 
// FUNCTION  cleanUp :                      
//     Detach and free shared memory             
// PARAMETER USAGE :                                           
//    memPtr - pointer to shared memory
//    memId - Id of shared memory              
// FUNCTION CALLED :                                           
//    none       
// ----------------------------------------------------------- 
void cleanUp(int* memPtr, int memId){
    //Detach Memory
    if(shmdt(memPtr) == 0)
        printf("*** MAIN: shared memory successfully detached\n");
    else
        printf("*** MAIN: error detaching shared memory\n");
        
    //Remove shared memory
    if(shmctl(memId, IPC_RMID, NULL) != -1)
        printf("*** MAIN: shared memory successfully removed\n");
    else
        printf("*** MAIN: error removing shared memory\n");
}
