// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 11/10/2010                                       
// PROGRAM ASSIGNMENT 4                                        
// FILE NAME : compute.c            
// PROGRAM PURPOSE :                                           
//    Matrix Multiplication with Unix Shared Memory      
// ----------------------------------------------------------- 

#include  <sys/types.h>
#include  <sys/ipc.h>
#include  <sys/shm.h>

#include <stdio.h>
#include <stdlib.h>

// ----------------------------------------------------------- 
// FUNCTION  main :                     
//     compute the value of 1 entry in result matrix                            
// PARAMETER USAGE :                                           
//    argc - number of args
//    argv - passed in args               
// FUNCTION CALLED :                                           
//   none          
// ----------------------------------------------------------- 
main(int argc, char *argv[]){
    key_t sharedMemKey;
    size_t sharedMemSize;
    int sharedMemId;
    int *sharedMemPtr;

    printf("   ### PROC(%d): entering with row %d and column %d\n",getpid(),*argv[1],*argv[2]);
    
    sharedMemKey = 2010;
    sharedMemSize = ((200 + 100 + 4)*sizeof(int));
    sharedMemId = shmget(sharedMemKey,sharedMemSize,IPC_CREAT | 0666);
    sharedMemPtr = (int *) shmat(sharedMemId, NULL, 0);  /* attach */
    
    int row = *argv[1];
    int col = *argv[2];
    int l = sharedMemPtr[0];
    int m = sharedMemPtr[1];
    int u = sharedMemPtr[l*m+2];
    int v = sharedMemPtr[l*m+3];
    
    //Find sum
    int sum,i,j;
    for(i=0;i<sharedMemPtr[1];i++){
        sum+=sharedMemPtr[2+row*v+i]*sharedMemPtr[2+l*m+i*v+i];
    }
    sharedMemPtr[2 + l*m + 2 + u*v + row*v + col] = sum;    
    printf("   ### PROC(%d): C[%d,%d] = %d\n",getpid(),*argv[1],*argv[2],sum);
    
    exit(1);
}
//	sharedMemPtr[row+i]*sharedMemPtr[sharedMemPtr[0]*sharedMemPtr[1]+4+row+i]
