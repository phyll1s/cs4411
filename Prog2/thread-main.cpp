// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 10/15/2010                                      
// PROGRAM ASSIGNMENT 2                                       
// FILE NAME : thread-main.cpp            
// PROGRAM PURPOSE :
//  Synchronize the eating, feeding, hunting, and sleeping of
//  a family of eagles. There is one mother eagle represented by a thread,
//  and m baby eagles each represented by their own thread. All activity is 
//  concurrently run.                                                 
// -----------------------------------------------------------

    #include "thread.h"
    #include <sstream>
    #include <iostream>
    #include <cstdlib>
    #include <string.h>
    using namespace std;
    
    // ----------------------------------------------------------- 
    // FUNCTION  main :                        
    //      Sets up variables, semaphores, and mutexes - creates, exexutes, and joins threads
    //      The driver of the program                  
    // PARAMETER USAGE :                                           
    //     int argc - the number of arguments passed in
    //     char* argv[] - passed in arguments              
    // FUNCTION CALLED :                                           
    //     none.          
    // ----------------------------------------------------------- 
        
    int main (int argc, char* argv[]){
        //Initialize variables
        int m = atoi(argv[1]);
        int n = atoi(argv[2]);
        int t = atoi(argv[3]);
        stringstream output;
        int currPot = 0;
        int wakingMom = 0;
        
        //Semaphores & Mutexes
        Semaphore maxFeeding("maxFeeding",m);
        Semaphore wakeMom("wakeMom",1);
        Semaphore waitForFood("waitForFood",1);
        
        
        //Array of mutexes for pots
        Mutex* pots[m];
        for(int i=0;i<m;i++){
            pots[m] = new Mutex("pot");
        }
        Mutex currPotLock("currPotLock");
          
        //Ouput initial text
        output << "MAIN: There are " << n << " baby eagles, "
               << m << " feeding pots, and " << t << " feedings." << endl;
        cout << output.str();
        output.str("");
        
        output << "MAIN: Game starts!!!!!" << endl;
        cout << output.str();
        output.str("");
        
        //Create and initialize threads
        MotherThread * mother = new MotherThread(t,waitForFood);
        mother->Begin();
        
        BabyThread * babies[n];
        for(int i=0; i<n; i++){
            babies[i] = new BabyThread(i+1,m,maxFeeding,pots,currPot,wakeMom,waitForFood,wakingMom,currPotLock);
            babies[i]->Begin();
        }
        
        //Cleanup before exit
        for(int i=0; i<n; i++){
            babies[i]->Join();
        }        
        delete[] pots,babies;
        delete mother;
        
        exit(0);
    }
