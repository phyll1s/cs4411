// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 10/15/2010                                      
// PROGRAM ASSIGNMENT 2                                       
// FILE NAME : thread.cpp            
// PROGRAM PURPOSE :
//  Synchronize the eating, feeding, hunting, and sleeping of
//  a family of eagles. There is one mother eagle represented by a thread,
//  and m baby eagles each represented by their own thread. All activity is 
//  concurrently run.                                                    
// ----------------------------------------------------------- 
    #include <sstream>
    #include <iostream>
    #include <string>
    using std::string;
    using namespace std;
    #include <ThreadClass.h>
    #include "thread.h"
    #include "thread-support.cpp"

    /****************************/
    /*Baby Thread               */
    /****************************/
    // ----------------------------------------------------------- 
    // FUNCTION  BabyThread::BabyThread :                         
    //     The constructor of the baby thread - used to initialize variables
    // PARAMETER USAGE :                                           
    //    int i - the baby's number 
    //    int np - The total number of pots
    //    Semaphore &m - the max number of babies to feed at one time
    //                  also the number of pots
    //    Mutex* p[] - an array of pots
    //    int cp - an index to the current pot to feed from
    //    Semaphore &wm - a semaphore that allows 1 baby to wake mom at a time
    //    Semaphore &wff - a semaphore that blocks babies from eating while mom is hunting
    //    int wakem - a flag that lets one baby wake the mom at a time
    //    Mutex &cpl - a mutex lock that allows the current pot index to be safely incremented 
    //    Semaphore &mf - a semaphore that only allows m babies to feed at once.
    // FUNCTION CALLED :                                           
    //    None.      
    // ----------------------------------------------------------- 
    BabyThread::BabyThread(int i, int np, Semaphore &m, Mutex* p[], int cp, Semaphore &wm, Semaphore &wff, int wakem, Mutex &cpl){
        num = i;
        numPots = np;
        maxFeeding = &m;
        pots = p;
        currPot = cp;
        wakeMom = &wm;
        waitForFood = &wff;
        wakingMom = wakem;
        currPotLock = &cpl;
    }
    
    // ----------------------------------------------------------- 
    // FUNCTION  BabyThread::ThreadFunc() :                        
    //     Contains the code run in the baby thread                            
    // PARAMETER USAGE :                                           
    //     none.               
    // FUNCTION CALLED :                                           
    //     Delay() 
    //     ready_to_eat() 
    //     finish_eating()      
    // ----------------------------------------------------------- 
    void BabyThread::ThreadFunc(){
        Thread::ThreadFunc();
        
        //Output init text
        for(int j=0; j<num; j++){
            output << " ";
        } 
        output << "Baby eagle " << num << " started." << endl;
        cout << output.str();
        output.str("");
        
        //Continuous loop to emulate baby eagle
        // play-hungry-eat-finish-other cycle
        while(1){
            /*Playing*/
            Delay();
            
            /*Hungry*/
            ready_to_eat(num,numPots,*maxFeeding,pots,currPot,*wakeMom,*waitForFood,wakingMom,*currPotLock);
                                            
            /*Eating*/
            Delay();
                        
            /*Finished Eating*/
            finish_eating(num,*maxFeeding); 
        }
    }
    
    /****************************/
    /*Mother Thread             */
    /****************************/
    // ----------------------------------------------------------- 
    // FUNCTION  MotherThread::MotherThread(int t, Semaphore &wff) :                        
    //     The constructor of the mother thread - used to initialize variables                           
    // PARAMETER USAGE :                                           
    //    int t - max number of feedings
    //    Semaphore &wff - a semaphore that blocks babies from eating while mother feeds               
    // FUNCTION CALLED :                                           
    //    none.          
    // ----------------------------------------------------------- 

    MotherThread::MotherThread(int t, Semaphore &wff){
        turns = t;
        waitForFood = &wff;
    }
    // ----------------------------------------------------------- 
    // FUNCTION   MotherThread::ThreadFunc() :                           
    //     Contains the code run in the mother thread                            
    // PARAMETER USAGE :                                           
    //    none.              
    // FUNCTION CALLED :                                           
    //    goto_sleep()
    //    Delay()
    //    food_read()
    //    Exit()        
    // -----------------------------------------------------------     
    void MotherThread::ThreadFunc(){
        Thread::ThreadFunc();                
        int feedings = 1;
                
        //Output init text
        output << "Mother eagle started." << endl;
        cout << output.str();
        output.str("");
        
        //Continuous loop while not on vacation to emulate 
        //mother eagle sleep-hunt-feed-sleep cycle
        while(feedings <= turns){
            /*Napping*/
            goto_sleep(*wakeMom);
            
            /*Hunting*/
            Delay();
            
            /*Food is ready*/
            food_ready(feedings,*waitForFood);
            
            /*Doing something else*/
            Delay();
            
            feedings++;
        }
        
        //Mother is outta here!
        output << "Mother eagle retires after serving " << turns << " feedings. Game ends!!!" << endl;
        cout << output.str();
        output.str("");
        Exit();
    }
