// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 10/15/2010                                      
// PROGRAM ASSIGNMENT 2                                       
// FILE NAME : thread-support.cpp            
// PROGRAM PURPOSE :
//  Synchronize the eating, feeding, hunting, and sleeping of
//  a family of eagles. There is one mother eagle represented by a thread,
//  and m baby eagles each represented by their own thread. All activity is 
//  concurrently run.                                                    
// -----------------------------------------------------------
    
    // ----------------------------------------------------------- 
    // FUNCTION  ready_to_eat :                         
    //     To coordinate the eating of the concurrent baby threads                           
    // PARAMETER USAGE :                                           
    //    int num - the baby's number 
    //    int numPots - The total number of pots
    //    Semaphore &maxFeeding - the max number of babies to feed at one time
    //                  also the number of pots
    //    Mutex** pots - an array of pots
    //    int currPot - an index to the current pot to feed from
    //    Semaphore &wakeMom - a semaphore that allows 1 baby to wake mom at a time
    //    Semaphore &waitForFood - a semaphore that blocks babies from eating while mom is hunting
    //    int wakingMom - a flag that lets one baby wake the mom at a time
    //    Mutex &currPotLock - a mutex lock that allows the current pot index to be safely incremented           
    // FUNCTION CALLED :                                           
    //    ready_to_eat          
    // ----------------------------------------------------------- 
    void ready_to_eat(int num, int numPots, Semaphore &maxFeeding, Mutex** pots, int currPot, Semaphore &wakeMom, Semaphore &waitForFood, int wakingMom, Mutex &currPotLock){
        stringstream output;
        int eatFrom;
        
        //Announce baby is ready to eat
        for(int i=0; i<num; i++){
            output << " ";
        } 
        output << "Baby eagle " << num << " is ready to eat." << endl;
        cout << output.str();
        output.str("");
        
        //Only allow m babies to eat at once
        maxFeeding.Wait();
        
        //Pause if food is being prepped
        waitForFood.Wait();
        
        //Inc. pot here so others can feed right away
        currPotLock.Lock();
        currPot++;
        currPotLock.Unlock();

        if(currPot-1<numPots){
            if(wakingMom){
                return ready_to_eat(num,numPots,maxFeeding,pots,currPot,wakeMom,waitForFood,wakingMom,currPotLock); 
            }
            (*pots[currPot-1]).Lock();
        
            //Announce which feeding pot baby is eating from
            for(int i=0; i<num; i++){
                output << " ";
            } 
            output << "Baby eagle " << num << " is eating using feeding pot " << eatFrom << "." << endl;
            cout << output.str();
            output.str("");
            
            
            (*pots[currPot-1]).Unlock();
            return;        
        }else{
            //Only 1 wakes mom
            waitForFood.Signal();
            //Currpot is now at the begining again
            currPotLock.Lock();            
            wakingMom = 1;
            currPot = 0;            
            wakeMom.Signal();            
            currPotLock.Unlock();
            
            ready_to_eat(num,numPots,maxFeeding,pots,currPot,wakeMom,waitForFood,wakingMom,currPotLock);   
        }

    }
    
    // ----------------------------------------------------------- 
    // FUNCTION  finish_eating :                       
    //     To allow announce a baby is done feeding and allow another in                            
    // PARAMETER USAGE :                                           
    //    int num - the baby's number
    //    Semaphore &maxFeeding - a semaphore that allows m babies to eat at once              
    // FUNCTION CALLED :                                           
    //    none.          
    // -----------------------------------------------------------  
    void finish_eating(int num,Semaphore &maxFeeding){
        stringstream output;
        
        //Announce baby is done eating
        for(int i=0; i<num; i++){
            output << " ";
        } 
        output << "Baby eagle " << num << " finishes eating. " << endl;
        cout << output.str();
        output.str("");  
        
        //Allow another baby to come in
        maxFeeding.Signal();    
    } 
    
    // ----------------------------------------------------------- 
    // FUNCTION  goto_sleep :                         
    //     Block the mother thread from running to emulate sleeping                           
    // PARAMETER USAGE :                                           
    //    Semaphore &wakeMom - The semaphore that blocks the mother thread               
    // FUNCTION CALLED :                                           
    //    none.          
    // ----------------------------------------------------------- 
    void goto_sleep(Semaphore &wakeMom){
        stringstream output;
        //Announce mother is napping
        output << "Mother eagle takes a nap." << endl;
        cout << output.str();
        output.str("");
        
        //Sleep until woken
        wakeMom.Wait();
        
    } 
    
    // ----------------------------------------------------------- 
    // FUNCTION  food_ready :                        
    //     Announce that food is preparred and signal the babies to eat                            
    // PARAMETER USAGE :                                           
    //    int feedings - the max number of feedings
    //    Semaphore &waitForFood - The semaphore that blocks the babies from eating while mother fills pots               
    // FUNCTION CALLED :                                           
    //    none.          
    // ----------------------------------------------------------- 
    void food_ready(int feedings,  Semaphore &waitForFood){
        stringstream output;        
                
        //Announce mother is done feeding and all pots are full
        output << "Mother eagle says \"Feeding (" << feedings << ")\"" << endl;
        cout << output.str();
        output.str("");
        
        waitForFood.Signal();
    }
