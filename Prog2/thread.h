// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 10/15/2010                                      
// PROGRAM ASSIGNMENT 2                                       
// FILE NAME : thread.h            
// PROGRAM PURPOSE :
//  Synchronize the eating, feeding, hunting, and sleeping of
//  a family of eagles. There is one mother eagle represented by a thread,
//  and m baby eagles each represented by their own thread. All activity is 
//  concurrently run.                                                    
// -----------------------------------------------------------
#ifndef thread_H_
#define thread_H_

#include <ThreadClass.h>
#include <sstream>

//Global functions & variables
void ready_to_eat(int num, int m, Semaphore &maxFeeding, Mutex** pots, int cp, Semaphore &wakeMom, Semaphore &waitForFood, int wakingMom, Mutex &currPotLock);
void finish_eating(int num, Semaphore &maxFeeding);
void goto_sleep(Semaphore &wakeMom);
void food_ready(int feedings, Semaphore *waitForFood);

class BabyThread : public Thread {
	
    public:
        void ThreadFunc();
        BabyThread(int i, int np, Semaphore &m, Mutex* p[], int cp, Semaphore &wm, Semaphore &wff, int wakem, Mutex & cpl);
		
	private:
	    int num,numPots,currPot;
	    stringstream output;
	    Semaphore * maxFeeding;
	    Mutex** pots;
	    Semaphore * wakeMom;
	    Semaphore * waitForFood;
	    int wakingMom;
	    Mutex * currPotLock;
};




class MotherThread : public Thread {

    public:
        void ThreadFunc();
        MotherThread(int t, Semaphore &wff);
    
    private:
        int turns;
        stringstream output;
	    Semaphore * waitForFood;
	    Semaphore * wakeMom;
};

#endif
