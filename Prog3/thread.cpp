// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 10/29/2010                                       
// PROGRAM ASSIGNMENT 3                                        
// FILE NAME : thread.cpp            
// PROGRAM PURPOSE :                                           
//    A solution to the River Crossing problem using monitors
//    to solve concurrency issues.        
// ----------------------------------------------------------- 

    #include <sstream>
    #include <iostream>
    #include <string>
    using std::string;
    using namespace std;
    #include <ThreadClass.h>
    #include "thread.h"
    #include "boat-monitor.h"

    /****************************/
    /*Boat Thread               */
    /****************************/
            
    BoatThread::BoatThread(int n, BoatMonitor &bm){
        maxTrips = n;
        boatMonitor = &bm;
    }    

    void BoatThread::ThreadFunc(){
        Thread::ThreadFunc();
        
        cout << "***** BOAT thread starts" << endl;
        
        //Loop to simulate boat actions
        while (1) {
            Delay();         // take a rest
            boatMonitor->BoatReady();     // ready for the next round
            Delay();         // row the boat
            boatMonitor->BoatDone();      // all people are on the other side
                             // come back for another river crossing 
        }
    }  
    
    
    /****************************/
    /*Cannibal Thread           */
    /****************************/
        
    CannibalThread::CannibalThread(int n, BoatMonitor &bm){
        num = n;
        boatMonitor = &bm;
        type = "c";
    }    

    void CannibalThread::ThreadFunc(){
        Thread::ThreadFunc();
        
        //Output init text
        for(int i=0; i<num; i++){
            output << " ";
        } 
        output << "Cannibal " << num << " starts" << endl;
        cout << output.str();
        output.str("");
        
        //Loop to simulate cannibal actions        
        while (1) {
            Delay();                            // take a rest 
            boatMonitor->CannibalArrives(num,*this);  // register to cross the river 
            Delay();                            // other stuffs 
                                                // come back for another river crossing
        }

    }    


    /****************************/
    /*Missionary Thread         */
    /****************************/
    
    MissionaryThread::MissionaryThread(int n, BoatMonitor &bm){
        num = n;
        boatMonitor = &bm;
        type = "m";
    }
    
    void MissionaryThread::ThreadFunc(){
        Thread::ThreadFunc();
                
        //Output init text
        for(int i=0; i<num; i++){
            output << " ";
        } 
        output << "Missionary " << num << " starts" << endl;
        cout << output.str();
        output.str("");
        
        //Loop to simulate missionary actions
        while (1) {
        Delay();                                    // take a rest                 
            boatMonitor->MissionaryArrives(num,*this);    // register to cross the river 
            Delay();                                // other stuffs 
                                                    // come back for another river crossing 
        }
                

    }

