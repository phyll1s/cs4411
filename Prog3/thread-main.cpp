// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 10/29/2010                                       
// PROGRAM ASSIGNMENT 3                                        
// FILE NAME : thread-main.cpp           
// PROGRAM PURPOSE :                                           
//    A solution to the River Crossing problem using monitors
//    to solve concurrency issues.        
// ----------------------------------------------------------- 

    #include "thread.h"
    #include "boat-monitor.h"
    #include <sstream>
    #include <iostream>
    using namespace std;

    BoatThread* boatThread;

    int main (int argc, char* argv[]){
        int c,m,b;   
        
        c = atoi(argv[1]);
        m = atoi(argv[2]);
        b = atoi(argv[3]);
        
        if(c==0){c = 8;}
        if(m==0){m = 8;}
        if(b==0){b = 5;}
        
        //Create and initialize the boat monitor
        BoatMonitor* boatMonitor = new BoatMonitor("boatMonitor",b);
        
        //Create and initialize the boath thread;
        boatThread = new BoatThread(b,*boatMonitor);
        boatThread->Begin();
        
        //Create and initialize the cannibal threads
        CannibalThread* cannThreads[c];
        for(int i=0; i<c; i++){
            cannThreads[i] = new CannibalThread(i+1,*boatMonitor);
            cannThreads[i]->Begin();
        }
        
        //Create and initialize the missionary threads
        MissionaryThread* missThreads[m];
        for(int i=0; i<m; i++){
            missThreads[i] = new MissionaryThread(i+1,*boatMonitor);
            missThreads[i]->Begin();
        }

        //Join the threads
        boatThread->Join();
        
        for(int i=0; i<c; i++){
            cannThreads[i]->Join();
        }
        
        for(int i=0; i<m; i++){
            missThreads[i]->Join();
        }
        
    }    
