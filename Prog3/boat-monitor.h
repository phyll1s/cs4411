// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 10/29/2010                                       
// PROGRAM ASSIGNMENT 3                                        
// FILE NAME : boat-monitor.h            
// PROGRAM PURPOSE :                                           
//    A solution to the River Crossing problem using monitors
//    to solve concurrency issues.        
// ----------------------------------------------------------- 


#ifndef boat-monitor_H_
#define boat-monitor_H_

#include <ThreadClass.h>
#include <sstream>
#include <vector>
#include "thread.h"

class BoatMonitor : public Monitor
{
     public:
          BoatMonitor(char* name, int b);          //constructor
          void CannibalArrives(int n, CannibalThread& ct);
          void MissionaryArrives(int n, MissionaryThread& mt);
          void BoatReady();
          void BoatDone();

     private:
        int trips,maxTrips,waitingCann,waitingMiss;
        int cannOnBoat,missOnBoat;
        stringstream output;
        Condition cannWaiting,missWaiting,riding,firstQueue;
        vector<Thread> gettingIn;
        
        void findLoad();
};
#endif
