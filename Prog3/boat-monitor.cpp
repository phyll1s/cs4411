// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 10/29/2010                                       
// PROGRAM ASSIGNMENT 3                                        
// FILE NAME : boat-monitor.cpp            
// PROGRAM PURPOSE :                                           
//    A solution to the River Crossing problem using monitors
//    to solve concurrency issues.        
// ----------------------------------------------------------- 

    #include "boat-monitor.h"
        BoatMonitor::BoatMonitor(char *Name, int b)
          : Monitor(Name, HOARE), cannWaiting("cannWaiting"), missWaiting("missWaiting"), riding("riding"), firstQueue("firstQueue"){
            
            MonitorBegin();
                maxTrips = b;
                trips = 0;
                waitingCann = 0;
                waitingMiss = 0;
            MonitorEnd();
    }


    void BoatMonitor::CannibalArrives(int n, CannibalThread& ct){
        MonitorBegin();
            //Wait to get in line for boat
            firstQueue.Wait();
            
            //Wait to be chosen from line of Cannibals
            waitingCann++;
            cannWaiting.Wait();
            gettingIn.push_back(&ct);
            
            //Wait to exit boat
            riding.Wait();
            waitingCann--;
        MonitorEnd();
        return;
    }


    void BoatMonitor::MissionaryArrives(int n, MissionaryThread& mt){
        MonitorBegin();
            //Wait to get in line for boat
            firstQueue.Wait();
            
            //Wait to be chosen from the line of missionaries
            waitingMiss++;
            missWaiting.Wait();
            gettingIn.push_back(&mt);
            
            //Wait to exit boat
            riding.Wait();
            waitingMiss--;
        MonitorEnd();
        return;
    }


    void BoatMonitor::BoatReady(){
        MonitorBegin();
            //If still moving people, do it again
            if(trips<=maxTrips){
                findLoad();                                
            }
                       
            //Otherwise print out closing remarks and exit
            else{
                output.str("");
                output << "MONITOR: " << maxTrips << " crosses have been made." << endl
                        << "MONITOR: This river cross is closed indefinitely for renovation." << endl;
                cout << output.str();
                output.str("");
                
                Exit();
            }
        MonitorEnd();
    }


    void BoatMonitor::BoatDone(){
        MonitorBegin();
            //Allow passengers to exit the vehicle
            for(int i=0; i<3; i++){
                riding.Signal();
            }
            
            output.str("");
            output << "***** Boat load (" << trips << "): Completed" << endl;
            cout << output.str();
            trips++ ;
        MonitorEnd();
        return;
    }
    
    
    void BoatMonitor::findLoad(){
        //Add people to lines until you have 2&1, or 3 of one 
        while(waitingMiss < 3 && waitingCann < 3){
            if(waitingMiss >=2 && waitingCann >=1)
                break;
            firstQueue.Signal();        
        }
        
        //2 missionaries and 1 cannibal
        if(waitingMiss >=2 && waitingCann >=1){
            Thread* temp;
            CannibalThread cannibal;
            vector<MissionaryThread> missionaries;

            missWaiting.Signal();
            missWaiting.Signal();
            cannWaiting.Signal();
            
            //Figure out which person of what type
            while(gettingIn.size()>0){
                temp = gettingIn.pop_back();
                if(temp->getType() == "c")
                    cannibal = *temp;
                else{
                    missionaries.push_back(*temp);
                }
            }
            
            //Output boat text
            output.str("");
            output << "MONITOR(" << trips << "): one cannibal (" << cannibal.getNum() << ") and two missionaries (" << missionaries.front().getNum() <<" , " << missionaries.back().getNum() << ") are selected" << endl;
            cout << output.str();
            
            output.str("");
            output << "***** Boat load (" << trips << "): Passenger list (c" << cannibal.getNum() << ", m" << missionaries.front().getNum() << ", m" << missionaries.back().getNum() << ")" << endl;
            cout << output.str();
            output.str(""); 
            
            
            
            return;
        }
        
        //3 missionaries
        else if(waitingMiss >= 3){
            for(int i=0; i<3; i++){
                missWaiting.Signal();
            }
            
            //Output boat text
            output.str("");
            output << "MONITOR(" << trips << "): three missionaries (" << gettingIn.at(0).getNum() << ", " << gettingIn.at(1).getNum() << ", " << gettingIn.at(2).getNum() <<") are selected" << endl;
            cout << output.str();
            
            output.str("");
            output << "***** Boat load (" << trips << "): Passenger list (m" << gettingIn.at(0).getNum() << ", m" << gettingIn.at(1).getNum() << ", m" << gettingIn.at(2).getNum() << ")" << endl;
            cout << output.str();
            output.str("");            
            
            return;
        }
        
        //3 cannibals
        else{
            for(int i=0; i<3; i++){
                cannWaiting.Signal();
            }

            //Output boat text
            output.str("");
            output << "MONITOR(" << trips << "): three cannibals (" << 1 << ", " << 2 << ", " << 3 <<") are selected" << endl;
            cout << output.str();
            
            output.str("");
            output << "***** Boat load (" << trips << "): Passenger list (c" << 1 << ", c" << 2 << ", c" << 3 << ")" << endl;
            cout << output.str();
            output.str("");            
            
            return;
        }
    
    }
    
    
    
