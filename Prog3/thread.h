// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 10/29/2010                                       
// PROGRAM ASSIGNMENT 3                                        
// FILE NAME : thread.h            
// PROGRAM PURPOSE :                                           
//    A solution to the River Crossing problem using monitors
//    to solve concurrency issues.        
// ----------------------------------------------------------- 

#ifndef thread_H_
#define thread_H_

#include <ThreadClass.h>
#include <sstream>

class BoatThread : public Thread {

    public:
        void ThreadFunc();
        BoatThread(int n, BoatMonitor &bm);
        
    private:
        int maxTrips;
        stringstream output;
        BoatMonitor* boatMonitor;
};

class CannibalThread : public Thread {
	
    public:
        void ThreadFunc();
        CannibalThread(int n, BoatMonitor &bm);
        int getNum(){return num;}
        char getType(){return type;}
		
	private:
	    int num;
	    char type;
        stringstream output;
        BoatMonitor* boatMonitor;  
};


class MissionaryThread : public Thread {

    public:
        void ThreadFunc();
        MissionaryThread(int n, BoatMonitor &bm);
        int getNum(){return num;}
        char getType(){return type;}
        
    private:
        int num;
        char type;
        stringstream output;
        BoatMonitor* boatMonitor;
};

#endif
