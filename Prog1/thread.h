// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 09/27/2010                                       
// PROGRAM ASSIGNMENT 1                                        
// FILE NAME : thread.h            
// PROGRAM PURPOSE :                                           
//      Find the maximum integer among n distinct integers of an array. 
//      The solution is found via a multi-threaded algorithm.
//      Step 1 will use n threads, step 2 n(n-1)/2 threads, and step 3 uses n
//         threads              
// ----------------------------------------------------------- 
#ifndef thread_H_
#define thread_H_
#include <ThreadClass.h>
class MaxThread : public Thread {
	
    public:
        MaxThread(int s, int i, int* compArr);    	//Constructor
        MaxThread(int s, int i, int* compArr, int* vArr);    //Constructor
        MaxThread(int s, int i, int j, int*compArr, int*vArr);  //Constructor	
		
	private:
        int step;
        int position;
        int x1,x2;
        int * compareArray;
        int * valueArray;
        void ThreadFunc();
};
#endif
