// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 09/27/2010                                       
// PROGRAM ASSIGNMENT 1                                        
// FILE NAME : thread-main.cpp            
// PROGRAM PURPOSE :                                           
//      Find the maximum integer among n distinct integers of an array. 
//      The solution is found via a multi-threaded algorithm.
//      Step 1 will use n threads, step 2 n(n-1)/2 threads, and step 3 uses n
//         threads              
// ----------------------------------------------------------- 

    #include "thread.h"
    #include <sstream>
    #include <iostream>
    using namespace std;

    // ----------------------------------------------------------- 
    // FUNCTION  main :                       
    //      Recieve input parameters, create threads to process data, display
    //      results                           
    // PARAMETER USAGE :                                           
    //      int argc - the number of input parameters
    //      char* argv[] - input parameters from command line                 
    // FUNCTIONS CALLED :                                           
    //      MaxThread::MaxThread(int s, int j, int* compArr, int* vArr);
    //      MaxThread::MaxThread(int s, int i, int j, int*compArr, int*vArr);    
    //      MaxThread::ThreadFunc();
    // ----------------------------------------------------------- 
    int main (int argc, char* argv[]){
        //Initialize variables
        int n = atoi(argv[1]);
	    int* values = new int[n];
	    //compareArray is an array of size n with 1s and 0s in it to designate which integer is largest
        int* compareArray = new int[n];
        stringstream output;
        
        //Output the number of input int's and their values
        output << "Number of input values = " << n << endl;
        output << "Input values         x = ";
        for(int i=2; i<argc; i++){
            values[i-2] = atoi(argv[i]);
            if(i<argc-1)            
                output<<values[i-2]<<" ";
            else
                output<<values[i-2]<<endl;            
        }
        cout << output.str();        
        //"Empty" the stringstream
        output.str("");     
        
        //--------------------------------
        //Step 1 -  Initialize Array of 1's
        //          Uses n threads
        //--------------------------------

        //Create and begin the threads        
        MaxThread* step1[n];
        
        for(int i=0; i<n; i++){
            step1[i] = new MaxThread(1,i,compareArray);
            step1[i]->Begin();
        }
        //Join the threads
        for(int i=0; i<n; i++){
            step1[i]->Join();
	    }

        //Print out initialized array
        output<<"After initialization w = ";
        for(int i=0; i<n; i++){
            if(i<n-1)
                output<<compareArray[i]<<" ";
            else
                output<<compareArray[i]<<endl;
        }
        cout << output.str();
        output.str(""); 
        
        //-------------------------------------------
        //Step 2 - Compare ints with n(n-1)/2 threads
        //-------------------------------------------

        //Create and begin the threads        
        MaxThread* step2[(n*(n-1)/2)];
        int threadNum = 0;
        for(int i=0; i<n; i++){
            for(int j=i+1; j<n; j++){
                step2[threadNum] = new MaxThread(2,i,j,compareArray,values);
                step2[threadNum]->Begin();
                threadNum++;
            }
        }
        //Join the threads
        for(int i=0; i<(n*(n-1)/2); i++){
            step2[i]->Join();
	    }
        
        //Print out compared array
        output<<"After Step 2         w = ";
        for(int i=0; i<n; i++){
            if(i<n-1)
                output<<compareArray[i]<<" ";
            else
                output<<compareArray[i]<<endl;
        }
        cout << output.str();
        output.str(""); 

        //------------------------------------------------------------------
        //Step 3 - Check the values of the compare array for the largest int
        //------------------------------------------------------------------
        
        //Create and begin the threads        
        MaxThread* step3[n];
        for(int i=0; i<n; i++){
            step3[i] = new MaxThread(3,i,compareArray,values);
            step3[i]->Begin();
        }
        //Join the threads
        for(int i=0; i<n; i++){
            step3[i]->Join();
	    }
	    return 0;
    }

    
