// ----------------------------------------------------------- 
// NAME : Andrew Markiewicz                  User ID: ajmarkie 
// DUE DATE : 09/27/2010                                       
// PROGRAM ASSIGNMENT 1                                        
// FILE NAME : thread.cpp            
// PROGRAM PURPOSE :                                           
//      Find the maximum integer among n distinct integers of an array. 
//      The solution is found via a multi-threaded algorithm.
//      Step 1 will use n threads, step 2 n(n-1)/2 threads, and step 3 uses n
//         threads              
// ----------------------------------------------------------- 

    #include <iostream>
    #include <sstream> 
	#include <string>
	using std::string;
    #include "thread.h"
    using namespace std;
    
    // ----------------------------------------------------------- 
    // FUNCTION  MaxThread(int s, int i, int* compArr):                     
    //     Constructor - initialize variables                          
    // PARAMETER USAGE :                                           
    //    int s - The step currently executing
    //    int i - This thread's position within the compare array
    //    int* compArr - A pointer to the compare array               
    // FUNCTION CALLED :                                           
    //    none      
    // ----------------------------------------------------------- 
    MaxThread::MaxThread(int s, int i, int* compArr){
        step = s;
        position = i;
        compareArray = compArr;
    }
    
    // ----------------------------------------------------------- 
    // FUNCTION  MaxThread(int s, int j, int* compArr, int* vArr):                     
    //     Constructor - initialize variables                          
    // PARAMETER USAGE :                                           
    //    int s - The step currently executing
    //    int i - This thread's position within the compare array
    //    int* compArr - A pointer to the compare array     
    //    int* vArr - A pointer to the array holding the integer values         
    // FUNCTION CALLED :                                           
    //    none      
    // -----------------------------------------------------------     
    MaxThread::MaxThread(int s, int i, int* compArr, int* vArr){
        step = s;
        position = i;
        compareArray = compArr;
        valueArray = vArr;
    }
    
    // ----------------------------------------------------------- 
    // FUNCTION  MaxThread(int s, int i, int j, int* compArr, int* vArr):                     
    //     Constructor - initialize variables                          
    // PARAMETER USAGE :                                           
    //    int s - The step currently executing
    //    int i - The first integer to compare
    //    int j - The second integer to compare
    //    int* compArr - A pointer to the compare array     
    //    int* vArr - A pointer to the array holding the integer values         
    // FUNCTION CALLED :                                           
    //    none      
    // -----------------------------------------------------------       
    MaxThread::MaxThread(int s, int i, int j, int* compArr, int* vArr){
        step = s;
        x1 = i;
        x2 = j;        
        compareArray = compArr;
        valueArray = vArr;
    }

    // ----------------------------------------------------------- 
    // FUNCTION  ThreadFunc():                       
    //     This function holds the code to run for the threads.
    //     Each thread executes the proper code based on the "step" variable
    // PARAMETER USAGE :                                           
    //    None.               
    // FUNCTION CALLED :                                           
    //    Thread::ThreadFunc() - required by ThreadMentor       
    // ----------------------------------------------------------- 
    void MaxThread::ThreadFunc(){
        Thread::ThreadFunc();
        stringstream output;
        
        //--------------------------------
        //Step 1 -  Initialize Array of 1's
        //--------------------------------
        if(step == 1){
            compareArray[position] = 1;
            Exit();
        }
        
        //-------------------------------------------
        //Step 2 - Compare 2 ints writing 0 in the compare array at the
        //          position of the smaller of the two ints
        //-------------------------------------------
        else if(step == 2){            
            if(valueArray[x1]<valueArray[x2]){
                compareArray[x1]=0;
                
                //Print what the thread compared and the results
                output << "Thread T(" << x1 << "," << x2 << ") compares x[" 
                << x1 << "] = " << valueArray[x1] << " and x[" << x2 << "] = " 
                << valueArray[x2] << " and writes 0 into w[" << x1 << "]" << endl;
                
                cout << output.str();
                //"Empty" the stringstream
                output.str("");  
                Exit();
            }else{
                compareArray[x2]=0;
                
                //Print what the thread compared and the results
                output << "Thread T(" << x1 << "," << x2 << ") compares x[" 
                << x1 << "] = " << valueArray[x1] << " and x[" << x2 << "] = " 
                << valueArray[x2] << " and writes 0 into w[" << x2 << "]" << endl;
                
                cout << output.str();
                output.str("");  
                Exit();
            }
        }
        
        //------------------------------------------------------------------
        //Step 3 - Check the value of the compare array this thread's position
        //------------------------------------------------------------------
        else if(step == 3){
            if(compareArray[position] == 1){
                output << "Maximum          = " << valueArray[position] << endl;
                output << "Location         = " << position << endl;
                
                cout<<output.str();
                output.str("");
                Exit();  
            }
            Exit();
        }
    }
